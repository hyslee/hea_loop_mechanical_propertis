# High entropy alloy, FeCrCoNiMn

**This research intends to study the interaction between a Sigma 3 grain boundary and dislocation loop in plastic deformation under tensile stress**

## Program Version

- Atomsk Version 0.11
- Ovtio Basic Verison 3.4.3

## Graphical Abstract

<a href=""><img src="/images/abstract.jpg" width="600" /></a>

**1. generate a Sigma 3 grain boundary using nickel atoms**

![alt text](/1-Generate_sigma3_GB_Ni/gb.png)

- The crystal orientations of each grain are extracted from the paper written by [Olmsted et al.](https://www.sciencedirect.com/science/article/pii/S1359645409002274)
 
--- 
     
    Uppper grain orientation 
    x_1 [1 1 -2]
    y_1 [1 1 1]
    z_1 [1 -1 0]

    Lower grain orientation
    x_2 [-1 -1 2]
    y_2 [1 1 1 ]
    z_2 [-1 1 0]

---

**2. Tile the structure to create the simulation that has the size large enough to produce meaningful results**

- Use Ovito modification 'replicate'
- Target simulation size is 100 x 600 x 100 Angstrom

![alt text](/1-Generate_sigma3_GB_Ni/gb_tile.png)

**3. generate HEA cantor alloy by randomly replacing atoms**

![alt text](/2-Generate_HEA/cantor.png)

**4. Convert LAMMPS data file into .cfg file**

- Atomsk only properly support .cfg data format for command line data modification
- LAMMPS data file format produce errors

``
    $ atomsk Cantor.data outputfilename.cfg
``
- this step temporarily replace atom types with impertinent elements (not important)

**5. Using Ovito, wrap the misplaced atoms into simulation box**

- Use Ovito modification 'Wrap at periodic boundaries'

![alt text](/4-wrap_atoms_N_export_data/wrap1.png)
![alt text](/4-wrap_atoms_N_export_data/wrap2.png)

**6. Extract the processed file in .data format**

**7. Convert .data file to .cfg file**

- step 6 and 7 are necessary due to the fact that Ovito does not support .cfg data format export

``
    $ atomsk input.data output.cfg
``

**8. rotate the simulation box**

- Simulation box must be rotated to align [111] plane normal to global Y axis (Atomsk dislocation loop command can only take X,Y, and Z as an argument)
- The simulation box is rotated from [11-2] [111] [1-10] to [100] [010] [001]

![alt text](/8-rotate_box/rot.png)

``
    $ atomsk filename.cfg -orient [11-2] [111] [1-10] [100] [010] [001] outputfilename.cfg
``

**9. add dislocation loop into the box (check if DXA detect dislocations)**
  
![alt text](/9-add_disloc_loop/dislocation1.png)
![alt text](/9-add_disloc_loop/dislocation2.png)

- Poisson's ratio of the high entropy alloy (0.256) is extracted from the paper written by [Huang et al.](https://www.sciencedirect.com/science/article/pii/S1359646215002596)
- [1 0 -1] is assigned as the Burgers vector of the dislocation loop
- Lattice parameter is 3.597 at 1543K from [Owen et al.](https://www.sciencedirect.com/science/article/pii/S1359645416307315)

``
    $ atomsk outputfilename.cfg -dislocation loop 0.6*box 0.6*box 0.6*box Y 30 1*a0*sqrt(2) -1*a0*sqrt(2) 0 0.256 outputfilename2.cfg
``

**10. rotate the box back to its original orientation**

![alt text](/10-rotate_back/rot_back.png)

``
    $ atomsk outputfilename2.cfg -orient [100] [010] [001] [11-2] [111] [1-10] outputfilename3.cfg
``

**11. Convert .cfg data into LAMMPS data file**

**12. Anneal and load the structure**
